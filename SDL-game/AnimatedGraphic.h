#ifndef __AnimatedGraphic__
#define __AnimatedGraphic__

#include <core/LoaderParams.h>
#include "SDLGameObject.h"
#include "GameObjectFactory.h"

class AnimatedGraphic : public SDLGameObject
{
public:
	AnimatedGraphic();
	virtual ~AnimatedGraphic();

	virtual void load(std::unique_ptr<LoaderParams> const &pParams);

	virtual void draw();
	virtual void update();
	virtual void clean();

private:
	int m_animationSpeed;
	int m_frameCount;
};

class AnimatedGraphicCreator : public BaseCreator
{
	virtual GameObject* createGameObject() const
	{
		return new AnimatedGraphic();
	}
};

#endif