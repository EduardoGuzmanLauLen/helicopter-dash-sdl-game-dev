#ifndef __GameStateMachine__
#define __GameStateMachine__

#include "GameState.h"
#include <vector>

class GameStateMachine
{
public:
	GameStateMachine() {}
	~GameStateMachine() {}

	void pushState(GameState* pState);
	void changeState(GameState* pState);
	void popState();

	void clean();

	void update();
	void render();

private:
	std::vector<GameState*> m_gameStates;
};

#endif