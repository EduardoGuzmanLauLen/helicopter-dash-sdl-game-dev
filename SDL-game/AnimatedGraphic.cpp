#include "AnimatedGraphic.h"
#include <SDL/SDL.h>

AnimatedGraphic::AnimatedGraphic() : SDLGameObject()
{

}

AnimatedGraphic::~AnimatedGraphic()
{
}

void AnimatedGraphic::draw()
{
	SDLGameObject::draw();
}

void AnimatedGraphic::update()
{
	m_currentFrame = int(((SDL_GetTicks() / (1000 / m_animationSpeed)) % m_numFrames));
}

void AnimatedGraphic::clean()
{
	SDLGameObject::clean();
}

void AnimatedGraphic::load(std::unique_ptr<LoaderParams> const &parameters)
{
	SDLGameObject::load(parameters);
	m_animationSpeed = parameters->getAnimationSpeed();
}
