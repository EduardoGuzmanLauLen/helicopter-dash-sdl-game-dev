#ifndef __Enemy__
#define __Enemy__

#include "SDLGameObject.h"

class Enemy : public SDLGameObject
{
public:

	virtual std::string type() { return "Enemy"; }

protected:

	int m_health;

	Enemy() : SDLGameObject() {}
	virtual ~Enemy() {}
};

#endif