# helicopter-dash
2D side scroller made with OpenGL (using SDL)

## Details
Made using C++ 14, SDL, zlib, tinyxml and base64. Only builds in Release for x64.

## TO-DO
1. Fix other build types (Debug) and versions (x86).
1. Use CMake to generate project.
